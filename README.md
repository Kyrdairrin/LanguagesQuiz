# LanguagesQuiz

## Summary

The app plays audio language files and the user have to guess which language it is.

![Screen][screen]

[screen]: public/assets/screen.png

The languages must be of the following format: *[code]_[name in english].mp3*. For example: *fr_french.mp3*.

## Downloads

[Download Windows installer](https://drive.google.com/file/d/17xZB2zBFOQZkJ7QdkU7ZTwZNLfwADdiG/view?usp=sharing)

[Download audio sample](https://drive.google.com/open?id=1kV6H6PY-surgO8fplfUpXT-TfIVzQcsN)

## Requirements

Node v8.9.1 or higher

## Dev commands

Project setup:

```
yarn install
```

Compile and hot-reload for development:

```
yarn serve
```

```
yarn serve:electron
```

Compile and minify for production:

```
yarn build
```

```
yarn build:electron
```

Lint and fix files:

```
yarn lint
```

Run unit tests:

```
yarn test:unit
```

## License

![AGPLv3][agpl]

The source code is licensed under the terms of the **GNU Affero General Public License v3.0**.

See [LICENSE.txt](LICENSE.txt) for details.

[agpl]: src/assets/agplv3-medium.png

## Credits

Project icon made by [Freepik](http://www.freepik.com) from [Flaticon](http://www.flaticon.com) is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/)
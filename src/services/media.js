import dataurl from 'dataurl';
import fs from 'fs';

export function audioDataUrlAsync(filePath) {
  const songPromise = new Promise((resolve, reject) => {
    fs.readFile(filePath, (err, data) => {
      if (err) { reject(err); }
      resolve(dataurl.convert({ data, mimetype: 'audio/mp3' }));
    });
  });
  return songPromise;
}

export function audioDataUrl(filePath) {
  const data = fs.readFileSync(filePath);
  return dataurl.convert({ data, mimetype: 'audio/mp3' });
}

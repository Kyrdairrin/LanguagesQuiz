import settings from 'electron-settings';
import fs from 'fs';
import * as path from 'path';
import { localize } from './localize';

export function getLanguageFiles() {
  const directory = settings.get('audio.directory');

  if (!fs.existsSync(directory)) return [];

  const files = fs.readdirSync(directory);
  return files.map((file) => {
    const filename = path.parse(file).name;
    return {
      code: filename.split('_')[0],
      name: localize(`lang:${filename.split('_')[1]}`),
      path: path.join(directory, file),
    };
  });
}

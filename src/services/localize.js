import i18next from 'i18next';
import settings from 'electron-settings';
import fr from './languages/fr.json';
import en from './languages/en.json';
import ja from './languages/ja.json';

const isDevelopment = process.env.NODE_ENV !== 'production';

function detectLanguage() {
  if (settings.has('language.code')) {
    return settings.get('language.code');
  }
  settings.set('language', { code: 'en' });
  return 'en';
}

function init() {
  i18next.init({
    lng: detectLanguage(),
    fallbackLng: 'en',
    ns: ['common', 'lang'],
    defaultNS: 'common',
    debug: isDevelopment,
    resources: { en, fr, ja },
  });
}

export function localize(key, interpolation = {}) {
  return i18next.t(key, interpolation);
}

export function localizeMany(keys) {
  return keys.map(localize);
}

export function getLanguages() {
  return [
    { label: 'English', code: 'en' },
    { label: 'Français', code: 'fr' },
    { label: '日本語', code: 'ja' },
  ];
}

init();

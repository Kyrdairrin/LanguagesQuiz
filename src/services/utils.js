/**
 * Produces a shuffled (Fisher-Yates) array.
 * DOES NOT modify the original array.
 *
 * @param {Array<T>} array The array to shuffle from
 * @returns {Array<T>} A new shuffled array
 */
export function shuffle(array) {
  const a = array.slice(0); // clone the array
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}

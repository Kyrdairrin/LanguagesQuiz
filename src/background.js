import { app, protocol, BrowserWindow, Menu, dialog, shell } from 'electron';
import settings from 'electron-settings';
import * as path from 'path';
import { format as formatUrl } from 'url';
import createProtocol from 'vue-cli-plugin-electron-builder/lib/createProtocol';
import { localize, getLanguages } from './services/localize';

const isDevelopment = process.env.NODE_ENV !== 'production';

// global reference to mainWindow (necessary to prevent window from being garbage collected)
let mainWindow;

// Standard scheme must be registered before the app is ready
protocol.registerStandardSchemes(['app'], { secure: true });
function createMainWindow() {
  const window = new BrowserWindow({
    title: localize('app_name'),
    icon: path.join(__static, 'icon.ico'),
    height: 595,
    width: 700,
    minHeight: 300,
    minWidth: 340,
  });

  Menu.setApplicationMenu(getMenu());

  if (isDevelopment) {
    // Load the url of the dev server if in development mode
    window.loadURL(process.env.WEBPACK_DEV_SERVER_URL);
  } else {
    createProtocol('app');
    //   Load the index.html when not in development
    window.loadURL(formatUrl({
      pathname: path.join(__dirname, 'index.html'),
      protocol: 'file',
      slashes: true,
    }));
  }

  window.on('closed', () => {
    mainWindow = null;
  });

  window.webContents.on('devtools-opened', () => {
    window.focus();
    setImmediate(() => {
      window.focus();
    });
  });

  return window;
}

// quit application when all windows are closed
app.on('window-all-closed', () => {
  // on macOS it is common for applications to stay open until the user explicitly quits
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // on macOS it is common to re-create a window even after all windows have been closed
  if (mainWindow === null) {
    mainWindow = createMainWindow();
  }
});

// create main BrowserWindow when electron is ready
app.on('ready', () => {
  mainWindow = createMainWindow();
});

function getMenu() {
  const currentLanguageCode = settings.get('language.code');
  const template = [
    {
      label: localize('menu.application'),
      submenu: [
        { label: localize('menu.new_game'), accelerator: 'CommandOrControl+N', click: () => mainWindow.reload() },
        { type: 'separator' },
        { label: localize('menu.restart'), accelerator: 'CommandOrControl+R', click: restart },
        { label: localize('menu.exit'), accelerator: 'CommandOrControl+Q', click: () => mainWindow.close() },
      ],
    },
    {
      label: localize('menu.settings'),
      submenu: [
        {
          label: localize('menu.language'),
          submenu: getLanguages().map(lang => ({
            label: lang.label,
            click: () => changeLanguage(lang.code),
            enabled: lang.code !== currentLanguageCode,
          })),
        },
        {
          label: localize('menu.audio_dir'),
          click: () => selectAudioDirectory(),
        },
      ],
    },
    {
      label: localize('menu.help'),
      submenu: [
        {
          label: localize('menu.how_to'),
          accelerator: 'F1',
          click: () => shell.openExternal('https://gitlab.com/Kyrdairrin/LanguagesQuiz/wikis/user-guide'),
        },
        { type: 'separator' },
        { label: localize('menu.about'), click: about },
        {
          label: localize('menu.source'),
          click: () => shell.openExternal('https://gitlab.com/Kyrdairrin/LanguagesQuiz'),
        },
      ],
    },
  ];

  if (isDevelopment) {
    template.push({
      label: 'DEV',
      submenu: [
        { role: 'toggledevtools' },
      ],
    });
  }

  return Menu.buildFromTemplate(template);
}

function selectAudioDirectory() {
  const paths = dialog.showOpenDialog(
    mainWindow,
    {
      title: localize('dialog.audio_dir.title'),
      properties: ['openDirectory'],
    },
  );

  if (paths === undefined) {
    return;
  }

  settings.set('audio', { directory: paths[0] });
  mainWindow.reload();
}

function changeLanguage(languageCode) {
  settings.set('language', { code: languageCode });
  dialog.showMessageBox(
    mainWindow,
    {
      message: localize('message.language_changed_restart'),
      buttons: [localize('yes'), localize('no')],
    },
    (id) => {
      if (id === 0) {
        restart();
      }
    },
  );
}

function restart() {
  app.relaunch();
  app.quit();
}

function about() {
  const window = new BrowserWindow({
    title: localize('about.title'),
    icon: path.join(__static, 'icon.ico'),
    parent: mainWindow,
    modal: true,
    height: 315,
    width: 500,
    resizable: false,
    minimizable: false,
    maximizable: false,
    show: false,
  });
  window.setMenu(null);

  if (isDevelopment) {
    window.loadURL(`${process.env.WEBPACK_DEV_SERVER_URL}/#/about`);
  } else {
    createProtocol('app');
    window.loadURL(`file://${__dirname}/index.html#about`);
  }

  window.once('ready-to-show', () => {
    window.show();
  });
}

import '@fortawesome/fontawesome-free/css/all.css';
import '@babel/polyfill';
import Vue from 'vue';
import './plugins/vuetify';
import App from './App.vue';
import router from './router';
import store from './store';
import { localize, localizeMany } from './services/localize';

Vue.config.productionTip = false;

Vue.mixin({
  filters: {
    localize,
    localizeMany,
  },
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');

/* eslint no-param-reassign: 0 */
import Vue from 'vue';
import Vuex from 'vuex';
import { getLanguageFiles } from './services/game';
import { shuffle } from './services/utils';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    questions: shuffle(getLanguageFiles()),
    index: 0,
    good: 0,
    mid: 0,
    bad: 0,
  },
  mutations: {
    next(state) {
      state.index++;
    },
    good(state) {
      state.good++;
    },
    mid(state) {
      state.mid++;
    },
    bad(state) {
      state.bad++;
    },
  },
  actions: {
    correct(context) {
      context.commit('good');
      context.commit('next');
    },
    wrong(context) {
      context.commit('bad');
      context.commit('next');
    },
    skip(context) {
      context.commit('mid');
      context.commit('next');
    },
  },
});

import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Welcome from './views/Welcome.vue';
import About from './views/About.vue';

Vue.use(Router);

export default new Router({
  mode: 'hash', // disable 'history' mode to allow accessing urls directly (from another window for example)
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/welcome/:uuid',
      name: 'welcome',
      component: Welcome,
      props: true,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
  ],
});

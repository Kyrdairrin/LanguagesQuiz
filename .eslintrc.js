const PROD = 'production';

module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    '@vue/airbnb'
  ],
  globals: {
    "__static": false
  },
  rules: {
    'no-console': process.env.NODE_ENV === PROD ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === PROD ? 'error' : 'off',
    'linebreak-style': [
      'error',
      'windows'
    ],
    quotes: [
      'error',
      'single',
      {
        avoidEscape: true
      }
    ],
    'no-use-before-define': [
      'error',
      {
        functions: false
      }
    ],
    'func-names': ['error', 'as-needed'],
    'no-plusplus': 'off',
    'space-before-function-paren': ['error', 'never'],
    'no-mixed-operators': ['error', {'allowSamePrecedence': true}],
    'no-nested-ternary': 'off',
    'import/no-extraneous-dependencies': ['error', { 'devDependencies': true }],
    'import/prefer-default-export': 'off',
    'vue/attribute-hyphenation': [
      'error',
      'always'
    ],
    'vue/html-indent': [
      'error',
      2
    ],
    'vue/require-default-prop': 'error',
    'vue/require-prop-types': 'error',
    'vue/attributes-order': 'error',
    'vue/html-quotes': [
      'error',
      'double'
    ],
    'vue/order-in-components': 'error'
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
};
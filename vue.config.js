module.exports = {
  pluginOptions: {
    electronBuilder: {
      builderOptions: {
        icon: './public/icon.ico',
        nsis: {
          oneClick: false,
          allowToChangeInstallationDirectory: true,
          license: './LICENSE.txt',
          multiLanguageInstaller: true,
          displayLanguageSelector: true,
          runAfterFinish: false,
          shortcutName: 'Languages Quiz',
        },
      },
    },
  },
};
